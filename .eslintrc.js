module.exports = {
  extends: [require.resolve('@umijs/lint/dist/config/eslint')],
  globals: {
    page: true,
    REACT_APP_ENV: true,
  },
  rules: {
    'react/jsx-uses-react': 'off',
    'react/react-in-jsx-scope': 'off',
    'jsx-quotes': 'off',
    'import/newline-after-import': 'off',
    'import/first': 'off',
    'import/no-commonjs': 'off',
    'react-hooks/exhaustive-deps': 'off',
    'no-param-reassign': 'off',
    '@typescript-eslint/no-unused-expressions': 'off', // 不允许使用表达式
  },
};
