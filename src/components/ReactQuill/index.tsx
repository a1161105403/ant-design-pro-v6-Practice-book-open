// RichTextEditor.tsx

import React, { useState } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; // 样式文件
import 'quill/dist/quill.js'; // Quill 主文件
import 'highlight.js/styles/monokai-sublime.css';
import highlight from 'highlight.js';
import './index.less';
interface RichTextEditorProps {
  onChange: (value: string) => void;
}

const RichTextEditor: React.FC<RichTextEditorProps> = ({ onChange }) => {
  const [content, setContent] = useState('');

  // 自定义工具栏
  const formats = [
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'video',
  ];

  const modules = {
    toolbar: {
      container: [
        // [{ 'header': 1 }, { 'header': 2 }], // 标题 —— 独立平铺
        [{ header: [1, 2, 3, 4, 5, 6, false] }], // 标题 —— 下拉选择
        [{ size: ['small', false, 'large', 'huge'] }], // 字体大小
        [{ list: 'ordered' }, { list: 'bullet' }], // 有序、无序列表
        ['blockquote', 'code-block'], // 引用  代码块
        // 链接按钮需选中文字后点击
        ['link', 'image', 'video'], // 链接、图片、视频
        [{ align: [] }], // 对齐方式// text direction
        [{ indent: '-1' }, { indent: '+1' }], // 缩进
        ['bold', 'italic', 'underline', 'strike'], // 加粗 斜体 下划线 删除线
        [{ color: [] }, { background: [] }], // 字体颜色、字体背景颜色
        [{ script: 'sub' }, { script: 'super' }], // 下标/上标
        [{ font: [] }], //字体
        ['clean'], // 清除文本格式
      ],
    },
    syntax: {
      highlight: (text: any) => {
        return highlight.highlightAuto(text).value; // 这里就是代码高亮需要配置的地方
      },
    },
  };

  const handleChange = (value: string) => {
    setContent(value);
    onChange(value);
  };

  return (
    <ReactQuill
      theme="snow"
      value={content}
      onChange={handleChange}
      formats={formats}
      modules={modules}
      style={{ height: '100%', width: '100%' }}
    />
  );
};

export default RichTextEditor;
