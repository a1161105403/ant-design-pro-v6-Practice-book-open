import React, { useEffect, useRef } from 'react';
import Typed from 'typed.js';
import { PropsType } from './index.d';
const TypeWriteEffect: React.FC<PropsType> = ({ text = '', callback, seed = 20 }) => {
  const el = useRef(null);
  useEffect(() => {
    const typed = new Typed(el.current, {
      strings: [text],
      typeSpeed: seed,
      showCursor: true,
      onComplete(self) {
        callback?.();
        self.cursor.style.display = 'none'; // 隐藏光标
      },
    });
    return () => {
      typed.destroy();
    };
  }, []);
  return (
    <div>
      <span ref={el}></span>
    </div>
  );
};
export default TypeWriteEffect;
