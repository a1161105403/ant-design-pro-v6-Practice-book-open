export type PropsType = {
  text: string; //文本内容
  seed?: number; //速度
  callback?: () => void; //打印结束后的回调函数
};
