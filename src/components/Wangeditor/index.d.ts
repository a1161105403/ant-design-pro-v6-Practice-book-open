export type InsertImgFnType = (url: string, alt: string, href: string) => void;
type InsertVidoeFnType = (url: string, poster: string = '') => void;
export type PropsType = {
  value?: string; //文本内容
  onChange?: (p: string) => void;
};
