import { IButtonMenu, IDomEditor } from '@wangeditor/editor';

class PrintMenu implements IButtonMenu {
  public title: string;
  public tag: string;
  // TS 语法

  constructor() {
    this.title = 'print12'; // 自定义菜单标题
    // this.iconSvg = '<svg>...</svg>' // 可选
    this.tag = 'button';
  }

  // 获取菜单执行时的 value ，用不到则返回空 字符串或 false
  getValue(editor: IDomEditor): string | boolean {
    console.log('getValue--', editor);

    // TS 语法
    // getValue(editor) {                              // JS 语法
    return ' hello ';
  }

  // 菜单是否需要激活（如选中加粗文本，“加粗”菜单会激活），用不到则返回 false
  isActive(editor: IDomEditor): boolean {
    console.log('isActive--', editor);
    // TS 语法
    // isActive(editor) {                    // JS 语法
    return false;
  }

  // 菜单是否需要禁用（如选中 H1 ，“引用”菜单被禁用），用不到则返回 false
  isDisabled(editor: IDomEditor): boolean {
    console.log('isDisabled--', editor);
    // TS 语法
    // isDisabled(editor) {                     // JS 语法
    return false;
  }

  // 点击菜单时触发的函数
  exec(editor: IDomEditor, value: string | boolean) {
    console.log('exec--editor', editor);
    console.log('exec--value', value);
    // TS 语法
    // exec(editor, value) {                              // JS 语法
    if (this.isDisabled(editor)) return;
    editor.insertText(value); // value 即 this.value(editor) 的返回值
  }
}

export default PrintMenu;
