export const getCenter = (lat: number, lng: number) => new TMap.LatLng(lat, lng);
// 默认经纬度
export const initLocation = {
  lat: 22.506492,
  lng: 113.939925,
};
// 将字符串转回经纬度
export const strTolatLng = (str: string) => {
  const [lng, lat] = str?.split(',');
  return {
    lat: Number(lat),
    lng: Number(lng),
  };
};
