export default {
  status: 0,
  message: 'query ok',
  count: 100,
  data: [
    {
      id: '10015633769202902297',
      title: '深圳腾讯滨海大厦',
      address: '广东省深圳市南山区海天二路33号',
      category: '房产小区:商务楼宇',
      type: 0,
      location: {
        lat: 22.522807,
        lng: 113.935338,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '15336847776077850441',
      title: '腾讯大厦',
      address: '广东省深圳市南山区深南大道10000号',
      category: '房产小区:商务楼宇',
      type: 0,
      location: {
        lat: 22.540366,
        lng: 113.934559,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '1971497702838974577',
      title: '腾讯众创空间(深圳)',
      address: '广东省深圳市南山区软件产业基地6栋3-9层',
      category: '公司企业:公司企业',
      type: 0,
      location: {
        lat: 22.522899,
        lng: 113.940873,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '6177569255449667420',
      title: '腾讯科技(深圳)有限公司(科兴科学园)',
      address: '广东省深圳市南山区科苑路科兴科学园C1-C2栋',
      category: '公司企业:公司企业',
      type: 0,
      location: {
        lat: 22.547916,
        lng: 113.944641,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '8234090150440726281',
      title: '深圳市腾讯计算机系统有限公司(飞亚达科技大厦)',
      address: '广东省深圳市南山区高新南一道2飞亚达科技大厦3层,5-10层,1202',
      category: '公司企业:公司企业',
      type: 0,
      location: {
        lat: 22.539335,
        lng: 113.95573,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '17572900980694086266',
      title: '腾讯WeSpace(深圳)',
      address: '广东省深圳市福田区深业上城CEEC(Loft-D4)4楼',
      category: '娱乐休闲:剧场音乐厅:剧场',
      type: 0,
      location: {
        lat: 22.555924,
        lng: 114.06954,
        height: 0,
      },
      adcode: 440304,
      province: '广东省',
      city: '深圳市',
      district: '福田区',
    },
    {
      id: '15683539451502199923',
      title: '腾讯滨海大厦-西门',
      address: '广东省深圳市南山区海天二路33号腾讯滨海大厦',
      category: '室内及附属设施:通行设施类:门/出入口',
      type: 0,
      location: {
        lat: 22.522399,
        lng: 113.935001,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '863908671581952796',
      title: '深圳腾讯滨海大厦地下停车场',
      address: '广东省深圳市南山区海天二路',
      category: '汽车:停车场',
      type: 0,
      location: {
        lat: 22.523237,
        lng: 113.935549,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '10755436196122034925',
      title: '腾讯前海',
      address: '广东省深圳市南山区前海湾地铁站B2出口',
      category: '房产小区:商务楼宇',
      type: 0,
      location: {
        lat: 22.537788,
        lng: 113.899126,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
    {
      id: '6956413758006162214',
      title: '腾讯科技(深圳)有限公司(松日鼎盛大厦)',
      address: '广东省深圳市南山区深南大道9996号松日鼎盛18楼',
      category: '公司企业:公司企业',
      type: 0,
      location: {
        lat: 22.54062,
        lng: 113.93278,
        height: 0,
      },
      adcode: 440305,
      province: '广东省',
      city: '深圳市',
      district: '南山区',
    },
  ],
  request_id: '9496116720872337333',
  id: 'cblopf44691',
};
