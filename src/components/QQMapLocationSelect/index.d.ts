import type { ProFormFieldItemProps, GroupProps, ProFormInstance } from '@ant-design/pro-form';

import type { InputProps, ColProps, FormInstance } from 'antd';
import type { InputRef } from 'antd/es/input';
export type LocationType = {
  lat: number;
  lng: number;
};
export interface PropsType extends ProFormFieldItemProps<InputProps, InputRef>, GroupProps {
  callback?: (lat: number, lng: number) => void; //下拉选中的回调函数
  qqMapKey: string; //腾讯地图key
  label?: string;
  name: string;
  colProps?: ColProps;
  value?: string; //当前值
  formRef: ProFormInstance;
}
