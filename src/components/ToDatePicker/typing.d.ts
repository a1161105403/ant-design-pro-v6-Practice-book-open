import { pickProFormItemProps, ProFormInstance } from '@ant-design/pro-components';
export interface ToDatePickerType extends ProFormFieldItemProps {
  label?: string;
  width?: number | 'sm' | 'md' | 'xl' | 'xs' | 'lg';
  name: string;
  formRef?: ProFormInstance | any;
  timePickerProps?: pickProFormItemProps;
  timeChange?: (d: string) => void;
}
