import { iconfontUrl } from '@/utils/config';
import { createFromIconfontCN } from '@ant-design/icons';
interface IconfontProps {
  type: string;
  className?: string;
  style?: React.CSSProperties;
}
const Iconfonts = createFromIconfontCN({
  // scriptUrl: process.env.NODE_ENV === 'production' ? iconfontFile : iconfontUrl,
  scriptUrl: iconfontUrl, // 在 iconfont.cn 上生成下载,
});
const Iconfont: React.FC<IconfontProps> = (props) => <Iconfonts {...props} />;
export default Iconfont;
