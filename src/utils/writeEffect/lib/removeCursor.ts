import initData from './tool';
/**
 * @description: //移除光标标签
 * @param {HTMLElement} dom //光标标签dom
 * @param {string} intervalName //定时器名字
 * @param {number} cursorAway //光标消失时间
 * @author: muge
 */
export const removeCursor = (dom: HTMLElement, intervalName: NodeJS.Timer, cursorAway: number) => {
  setTimeout(() => {
    clearInterval(intervalName);
    dom.removeChild(document.querySelector(`.${initData.cursorClassName}`) as HTMLElement);
  }, cursorAway);
};
