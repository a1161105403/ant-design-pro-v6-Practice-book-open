import initData from './tool';
/**
 * @description: //获取光标dom
 * @author: muge
 */
export const getCursorClassName = () => {
  return document.querySelector(`.${initData.cursorClassName}`) as HTMLElement;
};
