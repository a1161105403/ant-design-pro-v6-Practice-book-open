type cursorConfigType = {
  cursor?: boolean; //是否显示光标
  seed?: number; //光标默认速度=>默认250ms
  dieTime?: number; //打字结束后光标消失时间=>默认200ms
  blinkSeed?: number; //光标闪烁速度
};
export type TypingEffectType = {
  text: string; //文本
  seed?: number; //默认打字速度,默认250ms
  callback?: () => void; //打字机结束的回调函数
  cursorConfig?: cursorConfigType; //光标配置项
};
