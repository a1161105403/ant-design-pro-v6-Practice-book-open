import { Routes } from '../typings';
import type { Items, KeepElements, MenuItemType } from '../typings.d';
import { OperationType } from '../typings.d'

import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  CloseCircleOutlined,
  CloseOutlined,
  RedoOutlined,
} from '@ant-design/icons';

// 获取路由keepAlive为false的路径的函数
export const getKeepAlive = (routes: Routes): string[] => {
  const paths: string[] = [];
  Object.values(routes).forEach((route) => {
    if (route?.keepAlive) {
      paths.push(route.path);
    }
  });
  return paths;
};

// 右键菜单
export const menuItemRender: MenuItemType[] | any = (
  length: number,
  leftShow: boolean = true,
  rightShow: boolean = true,
) => [
    {
      label: '刷新',
      key: OperationType.REFRESH,
      icon: <RedoOutlined />,
    },
    length > 1 && {
      label: '关闭当前',
      key: OperationType.CLOSECRUTTNET,
      icon: <CloseOutlined />,
    },
    length > 1 && {
      label: '关闭其他',
      key: OperationType.CLOSEOTHER,
      icon: <CloseCircleOutlined />,
    },
    leftShow && {
      label: '关闭左侧',
      key: OperationType.CLOSELEFT,
      icon: <ArrowLeftOutlined />,
    },
    rightShow && {
      label: '关闭右侧',
      key: OperationType.CLOSERIGHT,
      icon: <ArrowRightOutlined />,
    },
  ]

// 排序对比重置当前tab列表
export const sortCurrentByItemsKeys = (keepElements: KeepElements, items: Items[]) => {
  const sortedElements: any = {};
  items.forEach((item: Items, index: number) => {
    const key = item.key;
    sortedElements[key] = keepElements.current[key];
    sortedElements[key].index = index; // 更新index值
  });
  keepElements.current = sortedElements
}