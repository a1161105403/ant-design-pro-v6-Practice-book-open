import type { ItemType } from 'rc-menu/lib/interface';
import { MutableRefObject, ReactNode } from 'react';

type Location = {
  pathname: string;
  search: string;
  hash: string;
  state: any;
  key: string;
};

type KeepElementsType = {
  pathname: string;
  children: ReactNode;
  closable: boolean;
  icon?: ReactNode;
  index: number;
  location: Location;
  name: string;
};

export type KeepElements = MutableRefObject<Record<string, KeepElementsType>>;

// 定义props的具体类型
export type layoutTabs = {
  isKeep: boolean;
  activeKey: string;
  tabNameMap: Record<string, number>;
  local: Record<string, string>;
  keepElements: KeepElements;
  navigate: (path: string) => void;
  dropByCacheKey: (path: string) => void;
  refreshTab: (path: string) => void;
  dropLeftTabs: (path: string) => void;
  dropRightTabs: (path: string) => void;
  dropOtherTabs: (path: string) => void;
  refreshTab: (path: string) => void;
  updateTab: (path: string, config: TabConfig) => void;
};

// 定义操作类型
export enum OperationType {
  REFRESH = 'refresh',
  CLOSECRUTTNET = 'close-current',
  CLOSEOTHER = 'close-other',
  CLOSELEFT = 'close-left',
  CLOSERIGHT = 'close-right',
}

export type MenuItemType = (ItemType & { key: OperationType }) | null;

export type MenuClickStrateg = {
  [key: string]: () => void;
};

export type Routes = {
  [key: string]: {
    path: string;
    keepAlive?: boolean;
  };
};
export type Items = {
  key: string;
  closable: boolean;
  label: ReactNode;
};
