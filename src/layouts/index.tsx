import { useOutlet } from '@umijs/max'
import styles from './index.less'
const Index = () => {
    const outlet = useOutlet()
    return (<div className={styles.layout}>
        {outlet}
    </div>);
}

export default Index;