import RichTextEditor from '@/components/ReactQuill';
import Wangeditor from '@/components/Wangeditor';
import React, { useEffect } from 'react';
import './index.less';
const Index: React.FC = () => {
  const handleEditorChange = (value: string) => {
    console.log('Editor Content:', value);
  };
  useEffect(() => {
    console.log('富文本');
  }, [])
  const str =
    '<p><em><strong>12311</strong></em></p><p>😀</p><p><span style="font-size: 32px; font-family: 华文仿宋;">45</span></p><p><img src="https://hwxc-test.oss-cn-shenzhen.aliyuncs.com/file/2023/11/16/e076e3cf-573a-4077-acb4-d31a6b1f2e83.png" alt="" data-href="" style="width: 210.00px;height: 210.00px;"/></p><p><br></p><div data-w-e-type="video" data-w-e-is-void><video poster="" controls="true" width="auto" height="200"><source src="https://hwxc-test.oss-cn-shenzhen.aliyuncs.com/file/2023/11/16/22f6a784-a8f1-4502-93e0-96612b51cfe4.mp4" type="video/mp4"/></video></div><p><br></p>';
  const str1 =
    '<p>富文本</p><table style="width: 100%;"><tbody><tr><th colSpan="1" rowSpan="1" width="74">开发</th><th colSpan="1" rowSpan="1" width="180">测试</th></tr><tr><td colSpan="1" rowSpan="1" width="auto"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;5</td><td colSpan="1" rowSpan="1" width="auto"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1</td></tr></tbody></table><p><br></p>';
  return (
    <div>
      {/* <RichTextEditor onChange={handleEditorChange} /> */}
      {/* <div dangerouslySetInnerHTML={{ __html: str }}></div> */}
      <div dangerouslySetInnerHTML={{ __html: str }}></div>
      {/* <Wangeditor /> */}
    </div>
  );
};

export default Index;
