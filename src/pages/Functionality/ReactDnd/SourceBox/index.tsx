import { useDrag } from 'react-dnd';
const ItemTypes = {
  BOX: 'box',
};
const style = {
  border: '1px dashed gray',
  backgroundColor: 'white',
  padding: '0.5rem 1rem',
  marginRight: '1rem',
  marginBottom: '1rem',
  cursor: 'move',
};
const SourceBox = ({ item, setCurrentList }: any) => {
  const [{ opacity }, drag] = useDrag(
    () => ({
      type: ItemTypes.BOX,
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.4 : 1,
      }),
      item: () => item, //返回当前列表项数据
      canDrag: (monitor) => {
        //是否取消拖拽
        console.log(monitor, 'monitor131');
        return true;
      },
      //
      end(currentItem, monitor) {
        // monitor.getDropResult(); //获取拖拽对象所处容器的数据
        // monitor.didDrop(); // 当前容器能否放置拖拽对象 拖动停止时触发
        monitor.didDrop() && setCurrentList(currentItem); //在容器点松开 才赋值
      },
    }),
    [],
  );
  return (
    <div ref={drag} style={{ ...style, opacity }}>
      {item.id}------{item.name}
    </div>
  );
};
export default SourceBox;
