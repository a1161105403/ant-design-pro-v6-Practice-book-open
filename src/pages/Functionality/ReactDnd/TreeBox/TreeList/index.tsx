/*
 * @Author: muge
 * @Date: 2021-12-07 19:43:57
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-12-07 19:56:03
 */
import React from 'react';
import { useDrag } from 'react-dnd';
const ItemTypes = {
  BOX: 'box',
};
const TreeList = (props: any) => {
  const { list } = props;
  const { key, title } = list;
  const [current, drag] = useDrag(
    () => ({
      type: ItemTypes.BOX,
      options: {
        // dropEffect: showCopyIcon ? 'copy' : 'move',
      },
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.4 : 1,
      }),
      item: () => list,
    }),
    [],
  );
  return (
    <div ref={drag} key={key}>
      {title}
    </div>
  );
};
export default TreeList;
