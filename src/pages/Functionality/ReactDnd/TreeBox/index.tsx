import { useDrag } from 'react-dnd';
import React from 'react';
import { Tree } from 'antd';
import TreeList from './TreeList';
const { TreeNode } = Tree;

const treeData = [
  {
    title: 'parent 1',
    key: '0-0',
    children: [
      {
        title: 'parent 1-0',
        key: '0-0-0',
        children: [
          {
            title: 'leaf',
            key: '0-0-0-0',
          },
          {
            title: 'leaf',
            key: '0-0-0-1',
          },
        ],
      },
      {
        title: 'parent 1-1',
        key: '0-0-1',
        children: [{ title: <span style={{ color: '#1890ff' }}>sss</span>, key: '0-0-1-0' }],
      },
    ],
  },
  {
    title: 'parent 2-1',
    key: '0-0-2',
    children: [{ title: <span style={{ color: '#1890ff' }}>sss</span>, key: '0-0-3-0' }],
  },
];
const ItemTypes = {
  BOX: 'box',
};
const TreeBox = () => {
  const [current, drag] = useDrag(
    () => ({
      type: ItemTypes.BOX,
      options: {
        // dropEffect: showCopyIcon ? 'copy' : 'move',
      },
      collect: (monitor) => ({
        // opacity: monitor.isDragging() ? 0.4 : 1,
      }),
      item: () => {},
    }),
    [],
  );
  const getTreeNode = (data: any) => {
    if (data && data.length > 0) {
      return data.map((item: any) => {
        if (item.children) {
          return (
            <TreeNode key={item.key} title={item.title}>
              {getTreeNode(item.children)}
            </TreeNode>
          );
        }
        // 自定义title 将其抽成一个组件即可
        return <TreeNode key={item.key} title={<TreeList list={item} />} />;
      });
    }
    return [];
  };
  return <Tree defaultExpandAll>{getTreeNode(treeData)}</Tree>;
};

export default TreeBox;
