import React from 'react';
import { useDrop } from 'react-dnd';
const ItemTypes = {
  BOX: 'box',
};

const style: any = {
  border: '1px solid gray',
  height: '15rem',
  width: '15rem',
  padding: '2rem',
  textAlign: 'center',
};
const TargetBox = ({ currentList }: any) => {
  const [{ isActive }, drop] = useDrop(() => ({
    accept: ItemTypes.BOX,
    collect: (monitor) => ({
      isActive: monitor.canDrop() && monitor.isOver(),
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
    // hover: (item, monitor) => {
    //   console.log(item, 'item');
    //   console.log(monitor, 'monitor');
    // },
  }));
  return (
    <div ref={drop} style={style}>
      {isActive ? 'Release to drop' : 'Drag item here'}
      <div
        style={{
          backgroundColor: 'pink',
          height: 30,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: 17,
          fontWeight: 600,
          width: '100%',
        }}
      >
        {JSON.stringify(currentList) !== '{}' ? JSON.stringify(currentList) : '当前item'}
      </div>
    </div>
  );
};

export default TargetBox;
