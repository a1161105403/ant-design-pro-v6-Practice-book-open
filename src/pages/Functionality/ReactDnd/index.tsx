import React, { useEffect, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import SourceBox from './SourceBox';
import TargetBox from './TargetBox';
import TreeBox from './TreeBox';
import { Select } from 'antd';
import { values } from 'lodash';
const item: any[] = [
  {
    id: 1,
    name: 'muge',
  },
  {
    id: 2,
    name: 'muxia',
  },
  {
    id: 3,
    name: 'mugege',
  },
];
const Index = () => {
  // 当前拖拽项
  const [currentList, setCurrentList] = useState<any>({});
  useEffect(() => {
    console.log('--ReactDnd组件刷新了');
  }, [])
  return (
    // 类似redux数据传输  需要在最外层包裹对象
    <DndProvider backend={HTML5Backend}>
      <h1>拖拽源组件 </h1>
      <h1>列表----------------树</h1>
      <div style={{ display: 'flex' }}>
        <div>
          {/* 列表拖拽源 */}
          {item.map((itemz: any, index: number) => (
            <SourceBox setCurrentList={setCurrentList} item={itemz} key={index} />
          ))}
        </div>
        {/* 注意，不要树组件整体直接设置拖拽，抽成一个组件来遍历每一项 =》自定义渲染*/}
        {/* 树形拖拽源 */}
        <TreeBox />
      </div>
      <h1>拖拽放置目标</h1>
      {/* 拖拽最终放置组件 */}
      <TargetBox currentList={currentList} />
      <Select
        options={[{
          label: '1',
          value: 1,
          h: 1
        }]}
        style={{ minWidth: '200px', textAlign: 'left' }}
        onChange={(v: string, b: any) => {
          console.log('num', b);
        }}
        showSearch
      />
    </DndProvider>
  );
};

export default Index;
