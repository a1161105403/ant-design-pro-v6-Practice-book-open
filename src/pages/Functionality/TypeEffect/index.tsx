import TypeWriteEffect from '@/components/TypeWriteEffect';
import { typingEffect } from '@/utils/writeEffect';
import React, { RefObject, useEffect, useRef } from 'react';

const Index = () => {
  const el = useRef<RefObject<HTMLElement> | any>();

  const richText =
    '原神 · 启动！<img src="https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2022%2F0830%2F74168ba1j00rhf6m5002cd000u000jfp.jpg&thumbnail=660x2147483647&quality=80&type=jpg" style="height: 150px"/><br/><hr><br><div>王者荣耀 · 启动！</div>';
  useEffect(() => {
    typingEffect(el.current, {
      text: richText,
      callback: () => {
        console.log('打印机结束后执行的回调函数！');
      },
      cursorConfig: {
        cursor: true,
      },
    });
  }, []);
  return (
    <>
      <TypeWriteEffect text={richText} />
      <br />
      <br />
      <br />
      <div ref={el}></div>
    </>
  );
};

export default Index;
