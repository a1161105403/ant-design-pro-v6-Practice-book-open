import { history, useModel } from "@umijs/max";
import { Button, Space } from "antd";

const Index = () => {
    const { closeTab } = useModel('useCustomTabs', (model) => ({
        closeTab: model.closeTab,
    }));
    return (<Space>
        <Button type='primary' onClick={closeTab}>关闭tabs</Button>
        <Button type='primary' onClick={() => history.push('/functionality/reactdnd?id=1')}>跳转/functionality/reactdnd?id=1</Button>
        <Button type='primary' onClick={() => history.push('/functionality/reactdnd?id=2')}>跳转/functionality/reactdnd?id=2</Button>
        <Button type='primary' onClick={() => history.push('/functionality/reactdnd?id=3')}>跳转/functionality/reactdnd?id=3</Button>
    </Space>);
}

export default Index;