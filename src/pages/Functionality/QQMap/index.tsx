import React, { useRef } from 'react';
import './index.less';
import type { ProFormInstance } from '@ant-design/pro-components';
import { ProForm } from '@ant-design/pro-components';
import QQMapLocationSelect from '@/components/QQMapLocationSelect';
import { qqMapKey } from '@/utils/config';
const Index = () => {
  const formRef = useRef<ProFormInstance>();
  const mapSelect = (lat: number, lng: number) => {
    formRef?.current?.setFieldsValue({
      map: `${lng},${lat}`,
    });
  };
  return (
    <>
      <ProForm
        formRef={formRef}
        onValuesChange={(changeValues) => {
          console.log(changeValues);
        }}
      >
        <QQMapLocationSelect
          formRef={formRef}
          label="经纬度"
          name="map"
          qqMapKey={qqMapKey}
          callback={mapSelect}
        />
      </ProForm>
    </>
  );
};
export default Index;
