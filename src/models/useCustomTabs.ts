import { layoutTabs } from '@/layouts/customTabs/typings';
import { history } from '@umijs/max';
import { useState } from 'react';
export default () => {
  const [tabInstance, setTabInstance] = useState<layoutTabs>();
  const closeTab = () => {
    const { dropByCacheKey, keepElements, navigate, activeKey } = tabInstance as layoutTabs;
    const pathList: string[] = Object.keys(keepElements.current);
    dropByCacheKey(activeKey);
    if (pathList?.length === 1) {
      // 如果只剩下一个tab，关闭后返回默认页，也可以不关闭最后一个tab，怎么写都行
      history.push('/admin');
      return;
    }
    const i = pathList.indexOf(activeKey);
    navigate(pathList[i === 0 ? i + 1 : i - 1]);
  };
  return { tabInstance, setTabInstance, closeTab };
};
